module.exports = {
  purge: ["./index.html"],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      fontFamily: {
        sans: ["Vazir", "Tahoma"],
      },
      colors: {
        brand: "#32b2c3",
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
};
